# project.py
from os import path
import time

from flow import FlowProject
from flow.environment import DefaultSlurmEnvironment
import mbuild as mb
import numpy as np
import gsd

import utils


scale_factor = 0.356
features = utils.features_dict
in_dir = "gsds"

quiet = False

class Project(FlowProject):
    pass


class Fry(DefaultSlurmEnvironment):
    hostname_pattern = "fry"
    template = "fry.sh"


@Project.label
def completed(job):
    """
    'box.txt' is the last file to be created in the coarse-graining and 'mol0.txt' existing means
    the initial frame is done, so once these files are made, the frame is done.
    """
    return job.isfile("box.txt") or job.isfile("mol0.txt")


@Project.label
def initialized(job):
    """
    'mol0.txt' must be created before coarse-graining can happen.
    """
    fname = job.sp["gsdfile"]
    proj = FlowProject.get_project()
    init_job = [job for job in proj.find_jobs(filter={"gsdfile" : fname, "frame": 0})][0]
    return init_job.isfile("mol0.txt")


@Project.operation
@Project.post(initialized)
def make_mol0(job):
    """
    Do initialization step only for frame 0 of trajectory
    """
    if job.sp["frame"] == 0:
        filename = job.fn("mol0.txt")
        if not path.exists(filename):
            start = time.perf_counter()
            gsdfile = f"{in_dir}/{job.sp['gsdfile']}"

            print(f"Initializing {job.sp['gsdfile']}...")

            f = gsd.pygsd.GSDFile(open(gsdfile, 'rb'))
            t = gsd.hoomd.HOOMDTrajectory(f)

            filename = job.fn("mol0.txt")

            print("Creating mol0.txt")
            # Get mol0
            comp0 = utils.CG_Compound.from_gsd(gsdfile, frame=0, scale=scale_factor)
            comp0.amber_to_element()
            mol0 = comp0.to_pybel()
            mol0.OBMol.PerceiveBondOrders()
            utils.save_mol_to_file(mol0,filename)

            end = time.perf_counter()
            print(f"Done! {end-start:.3f} seconds")

@Project.operation
@Project.pre(initialized)
@Project.post(completed)
def cg_frame(job):
    if job.sp["frame"] != 0:
        start = time.perf_counter()

        frame = job.sp["frame"]
        gsdpath = f"{in_dir}/{job.sp['gsdfile']}"
        proj = FlowProject.get_project()
        init_job = [job for job in proj.find_jobs(filter={"gsdfile" : job.sp["gsdfile"], "frame": 0})][0]
        init_file = init_job.fn("mol0.txt")

        if not quiet:
            print(f"Coarse-graining frame {frame}...")

        comp = utils.CG_Compound.from_gsd(gsdpath, frame=frame, scale=scale_factor)
        comp.amber_to_element()
        comp.unwrap()
        mol = comp.to_pybel(box=mb.Box(comp.box))
        mol.OBMol.PerceiveBondOrders()
        test = (
            mol.clone
        )  # I CANNOT EXPLAIN WHY THIS NEEDS TO BE HERE, BUT TRUST ME--IT DOES
        mol_fixed = utils.map_file_on_bad(init_file, mol)
        cg_comp_fixed = utils.coarse(
            mol_fixed, [features["thiophene"], features["alkyl_3"]]
        )
        cg_comp_fixed.wrap()

        end = time.perf_counter()

        if not quiet:
            print(f"Done! Coarse grained {job.sp['gsdfile']} in {end - start} seconds.\nStarting rdf...")

        for name in {p.name for p in cg_comp_fixed.particles()}:
            pos = np.stack([p.pos for p in cg_comp_fixed.particles() if p.name == name])
            pos_file = job.fn(f"pos_{name}.txt")
            np.savetxt(pos_file, pos)

        if not quiet:
            print("Done!\nStarting bond analysis...")

        particles = [p for p in cg_comp_fixed.particles()]
        bond_dict = cg_comp_fixed.find_bonds()
        for b_type in bond_dict:
            bond_file = job.fn(f"bond_{b_type[0]}{b_type[1]}.txt")
            with open(bond_file, "w") as f:
                for pair in bond_dict[b_type]:
                    if cg_comp_fixed.is_bad_bond(pair):
                        pos0 = particles[pair[0]].pos
                        pos1 = cg_comp_fixed.unwrap_position(pair)
                    else:
                        pos0 = particles[pair[0]].pos
                        pos1 = particles[pair[1]].pos
                    f.write(f"{utils.distance(pos0,pos1)}\n")

        if not quiet:
            print("Done!\nStarting angle analysis...")

        angle_dict = cg_comp_fixed.find_angles()
        for a_type in angle_dict:
            angle_file = job.fn(f"angle_{a_type[0]}{a_type[1]}{a_type[2]}.txt")
            with open(angle_file, "w") as f:
                for inds in angle_dict[a_type]:
                    pair1 = (inds[0], inds[1])
                    pair2 = (inds[1], inds[2])
                    bad1 = cg_comp_fixed.is_bad_bond(pair1)
                    bad2 = cg_comp_fixed.is_bad_bond(pair2)
                    if bad1 and bad2:
                        # the middle particle should be moved
                        pos0 = particles[inds[0]].pos
                        pos1 = cg_comp_fixed.unwrap_position(pair1)
                        pos2 = particles[inds[2]].pos

                    elif bad1:
                        # the first particle should be moved
                        pos0 = cg_comp_fixed.unwrap_position(pair1[::-1])
                        pos1 = particles[inds[1]].pos
                        pos2 = particles[inds[2]].pos

                    elif bad2:
                        # the last particle should be moved
                        pos0 = particles[inds[0]].pos
                        pos1 = particles[inds[1]].pos
                        pos2 = cg_comp_fixed.unwrap_position(pair2)
                    else:
                        # all good
                        pos0 = particles[inds[0]].pos
                        pos1 = particles[inds[1]].pos
                        pos2 = particles[inds[2]].pos

                    f.write(f"{utils.get_angle(pos0,pos1,pos2)}\n")

        np.savetxt(job.fn("box.txt"), cg_comp_fixed.box)

        if not quiet:
            print("Done!")


if __name__ == "__main__":
    Project().main()
