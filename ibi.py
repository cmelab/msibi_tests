import errno
import os

import freud
import gsd
import gsd.hoomd
import gsd.pygsd
import hoomd
import hoomd.md
import numpy as np
from matplotlib import pyplot as plt
from scipy import interpolate
from scipy.signal import savgol_filter


class IBI():

    dir_name = "msibi_output"

    try:
        os.mkdir(dir_name)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        pass

    def __init__(self):
        self.iteration = 0
        self.vstar = None
        self.vprev = None

    def gsd_rdf(self, gsdfile, start=None, stop=None, rmax=None, bins=65):
        """
        Calculate average rdf from gsd trajectory.
        """
        f = gsd.pygsd.GSDFile(open(gsdfile, "rb"))
        t = gsd.hoomd.HOOMDTrajectory(f)
        snap = t[0]
        if rmax is None:
            rmax = max(snap.configuration.box[:3]) / 2 - 1

        rdf = freud.density.RDF(bins, rmax)

        if stop is None:
            stop = len(t) - 1
        if start is None:
            start = stop - 5
        for frame in range(start, stop):
            snap = t[frame]
            box = freud.box.Box(*snap.configuration.box)
            n_query = freud.locality.AABBQuery.from_system((box, snap.particles.position))
            rdf.compute(n_query, reset=False)
        return rdf


    def extrapolate_V(self, r, rdf, smooth=True):
        """
        Use the first two nonzero values from the rdf
        to linearly extrapolate the potential and force to zero.
        """

        nonzero_inds = np.where(rdf > 0)[0][:2]

        x = r[nonzero_inds]
        y = -np.log(rdf[nonzero_inds])

        f = interpolate.interp1d(x, y, fill_value="extrapolate")

        v_r = np.where(rdf > 0, -np.log(rdf), f(r))

        if self.vstar is not None:
            v_r -= self.vstar

            try:
                v_r += self.vprev
            except TypeError:
                raise RuntimeError("If vstar is given, vprev must also be given")

        if smooth:
            min_val = min(v_r)
            min_ind = np.where(v_r==min_val)[0][0]
            ones = np.ones(min_ind)
            sigmoid = -1 / (1 + np.exp(-5 * np.linspace(-1, 1, len(v_r[min_ind:])))) + 1
            smoothing_arr = np.concatenate((ones, sigmoid))
            v_r *= smoothing_arr

        return v_r

    def extrapolate_F(self, r, rdf, v_r, smooth=True):
        """
        Use the first two nonzero values from the rdf
        to linearly extrapolate the force to zero.
        """

        nonzero_inds = np.where(rdf > 0)[0][:2]

        force = -np.gradient(v_r, r, edge_order=2)
        x = r[nonzero_inds]
        y = force[nonzero_inds]

        f = interpolate.interp1d(x, y, fill_value="extrapolate")

        f_r = np.where(rdf > 0, force, f(r))

        if smooth:
            min_val = min(f_r)
            min_ind = np.where(f_r==min_val)[0][0]
            ones = np.ones(min_ind)
            sigmoid = -1 / (1 + np.exp(-5 * np.linspace(-1, 1, len(f_r[min_ind:])))) + 1
            smoothing_arr = np.concatenate((ones, sigmoid))
            f_r *= smoothing_arr

        return f_r

    def get_table_potential(self, rdf, smooth=False, plot=False):
        """
        Use the rdf to get a tabulated potential for hoomd.pair.table
        """
        r = rdf.bin_centers

        rdf = rdf.rdf

        v = self.extrapolate_V(r, rdf)
        f = self.extrapolate_F(r, rdf, v)

        if smooth:
            v = savgol_filter(v, 9, 2)
            f = savgol_filter(f, 9, 2)

        if plot:
            plt.plot(r, v, label="V(r)")
            plt.plot(r, f, label="F(r)")
            #plt.plot(r, lj_potential(r), label="LJ")
            plt.xlabel("r")
            plt.ylabel("V(r)")
            plt.legend()

        v_array = np.stack((r, v, f), axis=1)

        return v_array


    def run_hoomd(self, run_steps=1e4, pot_file=None, gsd_init=None):
        hoomd.context.initialize("")

        if gsd_init is None:
            system = hoomd.init.create_lattice(unitcell=hoomd.lattice.sc(a=2.0), n=20)
        else:
            system = hoomd.init.read_gsd(gsd_init, frame=-1)

        nl = hoomd.md.nlist.cell()

        if pot_file is None:
            lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)
            lj.pair_coeff.set("A", "A", epsilon=1.0, sigma=1.0)
        else:
            file_len = sum(1 for line in open(pot_file)) - 1
            table = hoomd.md.pair.table(width=file_len, nlist=nl)
            table.set_from_file("A", "A", filename=pot_file)

        hoomd.md.integrate.mode_standard(dt=0.005)
        all = hoomd.group.all()
        nvt = hoomd.md.integrate.nvt(group=all, tau=1.0, kT=1)
        nvt.randomize_velocities(seed=42)

        log_name = f"{self.dir_name}/output-{self.iteration}.log"
        gsd_name = f"{self.dir_name}/trajectory-{self.iteration}.gsd"
        hoomd.analyze.log(
            filename=log_name,
            quantities=["potential_energy", "temperature"],
            period=100,
            overwrite=True,
        )
        hoomd.dump.gsd(gsd_name, period=5e2, group=all, overwrite=True)

        hoomd.run(run_steps)
        return log_name, gsd_name
