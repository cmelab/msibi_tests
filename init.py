# init.py
import os
from os import path
import time
import glob

import signac
import gsd
import gsd.hoomd

import utils


project = signac.init_project("p3ht-4")

# starting from the end of the trajectory, how many frames to average over to get the rdf/distributions
n_frames = 10

in_dir = "gsds"

for gsdpath in glob.glob(f"{in_dir}/*.gsd"):
    f = gsd.pygsd.GSDFile(open(gsdpath, 'rb'))
    t = gsd.hoomd.HOOMDTrajectory(f)
    gsdfile = os.path.basename(gsdpath)
    lastframe = len(t) - 1
    firstframe = lastframe - n_frames
    # need to do the first frame for initialization and the range
    for frame in [0] + [i for i in range(firstframe, lastframe)]:
        project.open_job({"frame": frame, "gsdfile": gsdfile}).init()
